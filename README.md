

    npm install --save @apollo/react-hooks @apollo/react-ssr apollo-boost graphql graphql-anywhere graphql-tag isomorphic-fetch react-apollo

## Change the API to yours in initApollo

    line 13: const GRAPHCMS_API = 'YOURAPIHERE'

## create a folder 'lib'

## add initApollo.js and withData.js

### for lists use plural js file, ie **recipes.js** for detail use **recipes/[id].js**

  
## List of something from pages/somethings.js
### You need these in the header

    import { graphql } from 'react-apollo'
    import gql from 'graphql-tag'
    import withData from '../lib/withData'


### you need the gql synstax you tested in GraphCMS

    const allRecipes = gql`
    	query allRecipes {
    		recipes(orderBy: createdAt_DESC) {
    			id
    			createdAt
    			title
    			description
    		}
    }`

### you need withData and graphql as a Higher Order Component to make this all work

    export default withData(graphql(allRecipes)(AllRecipes))

## Detail of something from /pages/somethings/[id].js

    import { graphql } from 'react-apollo'
    import gql from 'graphql-tag'
    import withData from '../../lib/withData' //notice the ../../ to go up another level

  
### you need the gql synstax you tested in GraphCMS

    const recipeDetails = gql`
	    query recipe($id: ID!) {
		    recipe(where: {id: $id})
		    {
			    id
			    title
			    description
			    prepTime
			    cookTime
			    ingredients {
				    id
				    name
			    }
		    }
	    }`

  

### then you need to pass the current ID from the URL

    export default withData(graphql(recipeDetails, {
    options: ({ url: { query: { id } } }) => ({ variables: { id } })
    })(Recipe))

This allows you to pass the URL data /recipes/**dasadasdasd** <-- as an ID via [id] holder into the view, then use this unique ID to grab the correct data from GraphCMS up in the gql