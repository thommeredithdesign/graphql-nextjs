import { graphql } from 'react-apollo'
import gql from 'graphql-tag'
import withData from '../lib/withData'

import MyLayout from '../components/MyLayout'
import Link from 'next/link'


const AllRecipes = ({data: { loading, error, recipes }}) => {
    if (error) return <h1>Error loading reviews.</h1>
    return (
        <MyLayout>
        {
            loading ? <div>Loading</div> :
            (
                <div>{recipes.map((recipe, index) => 
                    <div key={index}>
                        <h1>
                            <Link href='recipes/[id]' as={`recipes/${recipe.id}`}>
                                <a>{recipe.title}</a>
                            </Link>
                        </h1>
                        <h2>{recipe.description}</h2>
                    </div>
                )}</div>
            )
        }
        </MyLayout>
    )
    return null;
}

const allRecipes = gql`
    query allRecipes {
        recipes(orderBy: createdAt_DESC) {
            id
            createdAt
            title
            description
        }
    }`

export default withData(graphql(allRecipes)(AllRecipes))