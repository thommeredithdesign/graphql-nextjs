import { graphql } from 'react-apollo'
import gql from 'graphql-tag'
import withData from '../../lib/withData'



const Recipe = ({data: { loading, error, recipe }}) => {
    if (error) return <h1>Error loading reviews.</h1>
    return (
        <div>
        {
            loading ? <div>Loading</div> :
            (
            <div>
                <h1>{recipe.title}</h1>
                <h2>{recipe.description}</h2>
                <h3>Prep Time: {recipe.prepTime} min</h3>
                {recipe.ingredients.map((ingredient) =>
                    {ingredient.name}
                )}
            </div>
            )
        }
        </div>
    )
    return null;
}

const recipeDetails = gql`
    query recipe($id: ID!) {
        recipe(where: {id: $id})
        {
        id
        title
        description
        prepTime
        cookTime
        ingredients {
            id
            name
            }
        }
    }`


export default withData(graphql(recipeDetails, {
    options: ({ url: { query: { id } } }) => ({ variables: { id } })
})(Recipe))