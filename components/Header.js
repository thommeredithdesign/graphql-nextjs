import ActiveLink from './ActiveLink'

const linkStyle = {
  marginRight: 15
}

const Header = () => (
    <div>
        <ActiveLink href="/">
          <span>Home</span>
        </ActiveLink>
        <ActiveLink href="/recipes">
          <span>Recipes</span>
        </ActiveLink>
    </div>
)

export default Header
